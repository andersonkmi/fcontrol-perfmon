<%@ tag language="java" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Fcontrol</title>

<!-- Bootstrap Core CSS -->
<link href="<c:url value="/resources/css/bootstrap.min.css" />"
	rel="stylesheet">
<!-- Custom CSS -->
<link href="<c:url value="/resources/css/sb-admin.css"/>"
	rel="stylesheet">
<!-- Morris Charts CSS -->
<link href="<c:url value="/resources/css/plugins/morris.css"/>"
	rel="stylesheet">
<!-- Custom Fonts -->
	<link href="<c:url value="/resources/font-awesome/css/font-awesome.min.css"/>" rel="stylesheet" type="text/css">
	<link href="<c:url value="/resources/css/style.css" />" rel="stylesheet">
	
	<script src="<c:url value="/resources/scripts/jquery-2.1.4.js" />"></script>
	<script	src="<c:url value="/resources/scripts/jquery-ui-1.11.4/jquery-ui.js" />"></script>
	<script src="<c:url value="/resources/scripts/Chart.js" />"></script>
	<script src="<c:url value="/resources/scripts/legend.js" />"></script>
	<!-- Bootstrap Core JavaScript -->
	<script src="<c:url value="/resources/scripts/bootstrap.min.js"/>"></script>

	<script	src="<c:url value="/resources/scripts/Script.js" />"></script>
</head>
<body>

	<div id="wrapper">

		<!-- Navigation -->
		<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse"
				data-target=".navbar-ex1-collapse">
				<span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
			</button>
			<a class="navbar-brand logo" href="">Fcontrol</a>
		</div>
		<!-- Top Menu Items -->
		<ul class="nav navbar-right top-nav">

		</ul>
		<!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
		<div class="collapse navbar-collapse navbar-ex1-collapse">
			<ul class="nav navbar-nav side-nav">
				<li><a href="<c:url value="/graph/reportQuarterhour"/>""><i
						class="fa fa-fw fa-bar-chart-o"></i>Quantidade de transações</a></li>
				<li><a href="<c:url value="/graph/reportQuarterhourAmount"/>"><i class="fa fa-fw fa-table"></i>TPV das últimas 4 horas</a></li>
			</ul>
		</div>
		<!-- /.navbar-collapse --> </nav>

		<div id="page-wrapper">

			<div class="container-fluid">
				<jsp:doBody/>
				
			</div>
			<!-- /.container-fluid -->

		</div>
		<!-- /#page-wrapper -->

	</div>
	<!-- /#wrapper -->	
	
</body>

</html>
