<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags"%>

<t:template>

	<div class="row">
		<div class="col-lg-12">
			<h1>Quantidade de transa&ccedil;&otilde;es das &uacute;ltimas 4 horas</h1>
			<ol class="breadcrumb">
				<li class="active"><i class="fa fa-dashboard"></i></li>
			</ol>
		</div>
	</div>

	<div class="row">

		<table>
			<tr>
				<td><canvas id="canvas" height="500" width="970"></canvas>
					<div id="canvasLegend"></div></td>
			</tr>
		</table>
		<div id="lineLegend"></div>

	</div>
	<script type="text/javascript">
		function showLineChart() {

			$.ajax({
				type : "GET",
				url : "/fcontrol-perfmon-ws/services/quarterHourTransactionQuantity",
				cache : false,
				contentType : "application/json; charset=utf-8",
				success : function(response) {

					var responseLineGraph = jQuery.parseJSON(response);

					var ctx = document.getElementById("canvas")
							.getContext("2d");
					new Chart(ctx).Line(responseLineGraph);

					legend(document.getElementById("canvasLegend"),
							responseLineGraph);

				},
				error : function(response) {
					alert('Error while request..');

				}
			});

		}

		$(document).ready(function() {
			showLineChart();
			console.log("ready!");

			setInterval(function() {
				$("#canvas").empty();

				showLineChart();
			}, 30000);
		});
	</script>

</t:template>
