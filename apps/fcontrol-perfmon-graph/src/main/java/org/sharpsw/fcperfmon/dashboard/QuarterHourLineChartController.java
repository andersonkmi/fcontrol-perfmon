package org.sharpsw.fcperfmon.dashboard;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

import java.util.Calendar;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.sharpsw.fcperfmon.graph.QuarterHourGraphData;
import org.sharpsw.fcperfmon.services.QuarterHourGraphDataService;
import org.sharpsw.fcperfmon.utils.JsonUtil;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/graph")
public class QuarterHourLineChartController {
	private static final Logger logger = Logger.getLogger(QuarterHourLineChartController.class);
	
	@Resource
	private QuarterHourGraphDataService quarterHourGraphDataService;
	
	@Resource
	private JsonUtil<QuarterHourGraphData> jsonUtil;

	@RequestMapping(value = "/reportQuarterhour", method = GET)
	public String drawChart(ModelMap model) {
		return "QuarterHourLineGraph";
	}

	@RequestMapping(value = "/reportQuarterhourAmount", method = GET)
	public String drawChartAmount(ModelMap model) {
		return "QuarterHourLineGraphAmount";
	}
	
}
