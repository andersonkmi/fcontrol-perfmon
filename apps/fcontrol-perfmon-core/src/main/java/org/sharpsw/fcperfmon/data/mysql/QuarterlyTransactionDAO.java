package org.sharpsw.fcperfmon.data.mysql;

import static java.math.BigDecimal.ZERO;

import java.util.Collection;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.sharpsw.fcperfmon.services.CacheService;
import org.sharpsw.fcperfmon.services.MonitoringServiceType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class QuarterlyTransactionDAO extends MysqlBaseDAO<QuarterlyTransactionVO, Long> {
	@Resource
	private CacheService<QuarterlyTransactionVO> cacheService;
	
	@Autowired
	public QuarterlyTransactionDAO(DataSource mysqlDataSource) {
		super(QuarterlyTransactionDAO.class, mysqlDataSource);
	}

	@Override
	@Transactional("mysqlTxManager")
	public void save(QuarterlyTransactionVO entity) {
		saveTransaction(entity);
	}

	@Override
	@Transactional("mysqlTxManager")
	public void bulkSave(Collection<QuarterlyTransactionVO> items) {
		items.forEach(this::saveTransaction);
	}
	
	private void saveTransaction(QuarterlyTransactionVO item) {
		cacheService.put(item);
		String statement = "INSERT INTO trans_quarterly_monitor (user_id, calendar_id, quarter_hour_checkpoint_id, quantity, amount) VALUES (?, ?, ?, ?, ?)";
		getJdbcTemplate().update(statement, new Object[]{item.getUser(), item.getCalendar(), item.getQuarterHour(), item.getQuantity(), item.getAmount()});
	}

	@Transactional("mysqlTxManager")
	public QuarterlyTransactionVO findByUserAndDate(UserVO user, CalendarVO calendar, QuarterHourVO quarterHour) {
		QuarterlyTransactionVO result = cacheService.get(String.format("%s_%s_%s_%s",
				                                                       MonitoringServiceType.QUARTERLY_HOUR_MONITOR.getName(),
				                                                       user.getId().toString(),
				                                                       calendar.getId().toString(),
				                                                       quarterHour.getId().toString()),
				                                         QuarterlyTransactionVO.class);
		if(result != null) {
			return result;
		}

		try {
			String statement = "SELECT id, user_id, calendar_id, quarter_hour_checkpoint_id, quantity, amount FROM trans_quarterly_monitor WHERE user_id = ? AND calendar_id = ? AND quarter_hour_checkpoint_id = ?";
			result = getJdbcTemplate().queryForObject(statement, new QuarterlyTransactionRowMapper(), new Object[] { user.getId(), calendar.getId(), quarterHour.getId() });
		} catch (EmptyResultDataAccessException exception) {
			result = new QuarterlyTransactionVO();
			result.setUser(user.getId());
			result.setCalendar(calendar.getId());
			result.setQuarterHour(quarterHour.getId());
			result.setQuantity(0);
			result.setAmount(ZERO);
		}
		cacheService.put(result);
		return result;
	}

	@Override
	public QuarterlyTransactionVO findById(Long key) {
		return null;
	}

	@Override
	public Collection<QuarterlyTransactionVO> findAll() {
		return null;
	}

	@Override
	@Transactional("mysqlTxManager")
	public void update(QuarterlyTransactionVO entity) {
	}

	@Override
	@Transactional("mysqlTxManager")
	public void delete(QuarterlyTransactionVO entity) {
	}
}
