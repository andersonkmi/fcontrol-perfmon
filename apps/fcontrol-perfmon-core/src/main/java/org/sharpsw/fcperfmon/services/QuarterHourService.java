package org.sharpsw.fcperfmon.services;

import static java.util.Calendar.MILLISECOND;
import static java.util.Calendar.MINUTE;
import static java.util.Calendar.SECOND;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

@Service
public class QuarterHourService {
	private static final Logger logger = Logger.getLogger(QuarterHourService.class);
	SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
	
    public List<Calendar> obtainPreviousQuarters(Calendar reference, int numberOfHours) {
        List<Calendar> results = new ArrayList<>();

        Calendar currentDateTime = Calendar.getInstance();
        currentDateTime.setTimeInMillis(reference.getTimeInMillis());
        normalizeReferenceCalendar(currentDateTime);
        generateIntervals(currentDateTime, results, numberOfHours);
        return results;
    }

    private void normalizeReferenceCalendar(Calendar reference) {
        reference.set(MILLISECOND, 0);
        reference.set(SECOND, 0);
        int referenceMinute = reference.get(MINUTE);
        if(referenceMinute >= 45) {
            reference.set(MINUTE, 45);
        } else if(referenceMinute >= 30) {
            reference.set(MINUTE, 30);
        } else if(referenceMinute >= 15) {
            reference.set(MINUTE, 15);
        } else if(referenceMinute >= 0) {
            reference.set(MINUTE, 0);
        }
                
        logger.debug(String.format("Normalized hour: '%s'", sdf.format(reference.getTime())));
    }

    private void generateIntervals(Calendar reference, List<Calendar> results, int numberOfHours) {
        int TOTAL_NUMBER_QUARTER_POINTS_HOUR = 4;
        int totalNumberOfIntervalPoints = numberOfHours * TOTAL_NUMBER_QUARTER_POINTS_HOUR;
        for(int index = 0; index <= totalNumberOfIntervalPoints; index++) {
            Calendar point = Calendar.getInstance();
            point.setTimeInMillis(reference.getTimeInMillis());
            point.add(MINUTE, index * -15);
            results.add(point);
            logger.debug(String.format("Generated interval: '%s'", sdf.format(point.getTime())));
        }
        Collections.reverse(results);
    }
}
