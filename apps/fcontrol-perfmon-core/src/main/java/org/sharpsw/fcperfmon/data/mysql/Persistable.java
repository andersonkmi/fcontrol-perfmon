package org.sharpsw.fcperfmon.data.mysql;

public interface Persistable {
	public String cacheId();
}
