package org.sharpsw.fcperfmon.graph;

public class QuarterHourQuantityGraphDataSetQuantity extends QuarterHourQuantityDataSet<Integer> {
		
	public static QuarterHourQuantityGraphDataSetQuantity getInstance() {
		return new QuarterHourQuantityGraphDataSetQuantity();
	}
		
	private QuarterHourQuantityGraphDataSetQuantity() {
		super();
	}
		
}
