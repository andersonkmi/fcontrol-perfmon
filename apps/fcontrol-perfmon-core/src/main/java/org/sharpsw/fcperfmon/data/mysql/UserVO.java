package org.sharpsw.fcperfmon.data.mysql;

public class UserVO implements Persistable {
    private Long id;
    private String name;
    private boolean isDeviceFingerprintInUse;
    private boolean isMaster;
    private boolean isMonitoringEnabled;
    private boolean isReplacement;
    private String fillColor;
	private String highlightFill;
	private String highlightStroke;   
	
	private UserVO() {}
	
	public static UserVO getInstance() {
		UserVO instance = new UserVO();
		instance.id = new Long(0);
		instance.name = "";
		instance.fillColor = "";
		instance.highlightFill = "";
		instance.highlightStroke = "";
		return instance;
	}
	
    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setIsDeviceFingerprintInUse(boolean flag) {
        isDeviceFingerprintInUse = flag;
    }

    public boolean isDeviceFingerprintInUse() {
        return isDeviceFingerprintInUse;
    }

    public void setIsReplacement(boolean flag) {
        isReplacement = flag;
    }

    public boolean isReplacement() {
        return isReplacement;
    }
    
    public void setIsMaster(boolean flag) {
    	isMaster = flag;
    }
    
    public boolean isMaster() {
    	return this.isMaster;
    }
    
    public void setIsMonitoringEnabled(boolean flag) {
    	isMonitoringEnabled = flag;
    }
    
    public boolean isMonitoringEnabled() {
    	return isMonitoringEnabled;
    }

	public String getFillColor() {
		return fillColor;
	}

	public void setFillColor(String fillColor) {
		this.fillColor = fillColor;
	}

	public String getHighlightFill() {
		return highlightFill;
	}

	public void setHighlightFill(String highlightFill) {
		this.highlightFill = highlightFill;
	}

	public String getHighlightStroke() {
		return highlightStroke;
	}

	public void setHighlightStroke(String highlightStroke) {
		this.highlightStroke = highlightStroke;
	}

	@Override
	public String cacheId() {
		return "";
	}
    
}
