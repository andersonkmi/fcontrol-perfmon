package org.sharpsw.fcperfmon.data.mysql;

import java.util.Collection;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class UserDAO extends MysqlBaseDAO<UserVO, Long> {
	@Autowired
	public UserDAO(DataSource mysqlDataSource) {
		super(UserDAO.class, mysqlDataSource);
	}

	@Override
	@Transactional("mysqlTxManager")
	public void save(UserVO entity) {
		saveUser(entity);
	}

	@Override
	@Transactional("mysqlTxManager")
	public void bulkSave(Collection<UserVO> items) {
		items.forEach(this::saveUser);
	}
	
	
	private void saveUser(UserVO entity) {
		String statement = "INSERT INTO user (name, device_fingerprint_enabled, is_master, is_monitoring_enabled, is_exchange, fillColor_RGBA, highlightFill_RGBA, highlightStroke_RGBA) VALUES (?, ?, ?, ?, ? ,? ,?)";
		getJdbcTemplate().update(statement, new Object[] {entity.getId(), entity.getName(), entity.isDeviceFingerprintInUse(), entity.isMaster(), entity.isMonitoringEnabled(), entity.isReplacement(), entity.getFillColor(), entity.getHighlightFill(), entity.getHighlightStroke()});
	}

	@Override
	@Transactional("mysqlTxManager")
	public UserVO findById(Long key) {
		String statement = "SELECT id, name, device_fingerprint_enabled, is_master, is_monitoring_enabled, is_exchange, fillColor_RGBA, highlightFill_RGBA, highlightStroke_RGBA FROM user WHERE id = ?";
		UserVO user = getJdbcTemplate().queryForObject(statement, new UserRowMapper(), new Object[] { key });
		return user;
	}

	@Override
	@Transactional("mysqlTxManager")
	public Collection<UserVO> findAll() {
		logger.info("Finding all users");
		String statement = "SELECT id, name, device_fingerprint_enabled, is_master, is_monitoring_enabled, is_exchange, fillColor_RGBA, highlightFill_RGBA, highlightStroke_RGBA FROM user order by 1";
		Collection<UserVO> users = getJdbcTemplate().query(statement, new UserRowMapper());
		return users;
	}

	@Transactional("mysqlTxManager")
	public Collection<UserVO> findAllByMonitoringStatus(boolean status) {
		logger.info("Finding all users");
		String statement = "SELECT id, name, device_fingerprint_enabled, is_master, is_monitoring_enabled, is_exchange, fillColor_RGBA, highlightFill_RGBA, highlightStroke_RGBA FROM user WHERE is_monitoring_enabled = ?";
		Collection<UserVO> users = getJdbcTemplate().query(statement, new UserRowMapper(), new Object[] { status });
		return users;
	}

	@Transactional("mysqlTxManager")
	public Collection<UserVO> findAllEnabledMonitor() {
		return findAllByMonitoringStatus(true);
	}
	
	@Override
	@Transactional("mysqlTxManager")
	public void update(UserVO user) {
		String statement = "UPDATE user SET name = ?, device_fingerprint_enabled = ?, is_master = ?, is_monitoring_enabled = ?, is_exchange = ? WHERE id = ?";
		getJdbcTemplate().update(statement, new Object[] {user.getName(), user.isDeviceFingerprintInUse(), user.isMaster(), user.isMonitoringEnabled(), user.isReplacement(), user.getId()});
	}
	
	@Override
	@Transactional("mysqlTxManager")
	public void delete(UserVO user) {
		String statement = "DELETE FROM user WHERE id = ?";
		getJdbcTemplate().update(statement, new Object[] {user.getId()});
	}
}
