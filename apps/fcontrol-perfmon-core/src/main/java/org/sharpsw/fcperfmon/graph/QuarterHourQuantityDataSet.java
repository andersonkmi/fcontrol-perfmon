package org.sharpsw.fcperfmon.graph;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public abstract class QuarterHourQuantityDataSet<T> {
	
	private String label;
	private String fillColor;
	private String highlightFill;
	private String highlightStroke;
	private String pointColor;
	private String strokeColor;
	
	private List<T> data;
	
	protected QuarterHourQuantityDataSet(){
		data = new LinkedList<T>();
	}
	
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public String getFillColor() {
		return fillColor;
	}
	public void setFillColor(String fillColor) {
		this.fillColor = fillColor;
	}
	public String getHighlightFill() {
		return highlightFill;
	}
	public void setHighlightFill(String highlightFill) {
		this.highlightFill = highlightFill;
	}
	public String getHighlightStroke() {
		return highlightStroke;
	}
	public void setHighlightStroke(String highlightStroke) {
		this.highlightStroke = highlightStroke;
	}
	public String getPointColor() {
		return pointColor;
	}
	public void setPointColor(String pointColor) {
		this.pointColor = pointColor;
	}
	public String getStrokeColor() {
		return strokeColor;
	}
	public void setStrokeColor(String strokeColor) {
		this.strokeColor = strokeColor;
	}
	
	public void addData(T quantity) {
		data.add(quantity);
	}
			
	public List<T> getData() {
		return Collections.unmodifiableList(data);
	}
	
}
