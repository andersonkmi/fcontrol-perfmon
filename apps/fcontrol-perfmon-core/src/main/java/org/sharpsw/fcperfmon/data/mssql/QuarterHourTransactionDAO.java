package org.sharpsw.fcperfmon.data.mssql;

import java.util.Calendar;
import java.util.List;
import java.util.stream.Collectors;

import javax.sql.DataSource;

import org.sharpsw.fcperfmon.data.mysql.UserVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class QuarterHourTransactionDAO extends MSSQLBaseDAO {
	private static final Long FCONTROL_SYSTEM_ID = 0L;
	
	@Autowired
	public QuarterHourTransactionDAO(DataSource sqlServerDataSource) {
		super(QuarterHourTransactionDAO.class, sqlServerDataSource);
	}


	@Transactional("mssqlTxManager")
	public List<FControlRequestRecordVO> calculate(List<UserVO> users, Calendar start, Calendar end) {
		return users.stream().map(user -> calculateByUser(user, start, end)).collect(Collectors.toList());
	}
	
	private FControlRequestRecordVO calculateByUser(UserVO user, Calendar start, Calendar end) {						        
        if(user.getId() == FCONTROL_SYSTEM_ID) {
        	return calculateEntireSystem(start, end);
        } else if(user.isReplacement()) {
        	return calculateForExchangeUser(user, start, end);
        } else {
        	return calculateForEcommerceUser(user, start, end);
        }
	}

	private FControlRequestRecordVO calculateForEcommerceUser(UserVO user, Calendar start, Calendar end) {
		if(user.isMaster()) {
			return calculateMasterUser(user, start, end);
		}
		return calculateNormalUser(user, start, end);
	}

	private FControlRequestRecordVO calculateForExchangeUser(UserVO user, Calendar start, Calendar end) {
		logger.info(String.format("Pooling information for user '%d' - '%s'", user.getId(), user.getName()));
		Object[] args = new Object[] { user.getId(), start, end };
		String queryStatement = "SELECT ISNULL(COUNT(1), 0) as quantity, ISNULL(SUM(Ressarcimento_ValorReembolso/100.00), 0.00) as amount FROM [trans_troca] WITH(NOLOCK) WHERE user_id = ? AND DataAbertura BETWEEN ? AND ?";

		FControlRequestRecordVO result = getJdbcTemplate().queryForObject(queryStatement, args, new FControlRequestRecordRowMapper());
		result.setUserId(user.getId());
		return result;
	}
	
	private FControlRequestRecordVO calculateEntireSystem(Calendar start, Calendar end) {
    	logger.info("Calculating for the entire FControl system");
        Object[] args = new Object[] { start, end };
        String queryStatement = "SELECT ISNULL(COUNT(1), 0) as quantity, ISNULL(SUM(total/100.00), 0.00) as amount FROM [trans] WITH(NOLOCK) WHERE date_firstconsult BETWEEN ? AND ?";

        FControlRequestRecordVO result = getJdbcTemplate().queryForObject(queryStatement, args, new FControlRequestRecordRowMapper());
        result.setUserId(FCONTROL_SYSTEM_ID);
        return result;
	}
	
	private FControlRequestRecordVO calculateMasterUser(UserVO user, Calendar start, Calendar end) {
		logger.info(String.format("Pooling information for user '%d' - '%s'", user.getId(), user.getName()));
    	Object[] args = new Object[] { user.getId(), start, end };
    	String queryStatement = "SELECT ISNULL(COUNT(1), 0) as quantity, ISNULL(SUM(total/100.00), 0.00) as amount FROM [trans] WITH(NOLOCK) WHERE user_id = ? AND date_firstconsult BETWEEN ? AND ?";           		

    	FControlRequestRecordVO result = getJdbcTemplate().queryForObject(queryStatement, args, new FControlRequestRecordRowMapper());
        result.setUserId(user.getId());
        
        String queryStatementMaster = "SELECT ISNULL(COUNT(1), 0) as quantity, ISNULL(SUM(total/100.00), 0.00) as amount FROM [trans] t WITH(NOLOCK) INNER JOIN [user] u WITH(NOLOCK) ON t.user_id = u.user_id WHERE u.idMaster = ? AND date_firstconsult BETWEEN ? AND ?";
        FControlRequestRecordVO result2 = getJdbcTemplate().queryForObject(queryStatementMaster, args, new FControlRequestRecordRowMapper());
        
        result.setAmount(result.getAmount().add(result2.getAmount()));
        result.setQuantity(result.getQuantity() + result2.getQuantity());
        return result;		
	}
	
	private FControlRequestRecordVO calculateNormalUser(UserVO user, Calendar start, Calendar end) {
		logger.info(String.format("Pooling information for user '%d' - '%s'", user.getId(), user.getName()));
    	Object[] args = new Object[] { user.getId(), start, end };
    	String queryStatement = "SELECT ISNULL(COUNT(1), 0) as quantity, ISNULL(SUM(total/100.00), 0.00) as amount FROM [trans] WITH(NOLOCK) WHERE user_id = ? AND date_firstconsult BETWEEN ? AND ?";           		

    	FControlRequestRecordVO result = getJdbcTemplate().queryForObject(queryStatement, args, new FControlRequestRecordRowMapper());
        result.setUserId(user.getId());
        return result;
	}
}
