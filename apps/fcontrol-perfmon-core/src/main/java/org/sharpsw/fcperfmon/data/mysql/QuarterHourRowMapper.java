package org.sharpsw.fcperfmon.data.mysql;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.util.Calendar;

import org.springframework.jdbc.core.RowMapper;

public class QuarterHourRowMapper implements RowMapper<QuarterHourVO> {
	@Override
	public QuarterHourVO mapRow(ResultSet rs, int rowNum) throws SQLException {
		QuarterHourVO element = QuarterHourVO.getInstance();
		element.setId(rs.getInt("id"));
		Time time = rs.getTime("checkpoint");
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(time.getTime());
		element.setCheckpoint(calendar);
		return element;
	}

}
