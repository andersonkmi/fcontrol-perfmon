package org.sharpsw.fcperfmon.caching;

import java.util.Date;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

@Component
public class RedisCaching {
	private static final Logger logger = Logger.getLogger(RedisCaching.class);

	@Autowired
	private RedisTemplate<String, String> redisTemplate;

	public void set(String key, String json) {
		logger.info(String.format("Inserting into cache the key-value pair: '%s':'%s'", key, json));
		redisTemplate.opsForValue().set(key, json);
	}

	public String get(String key) {
		logger.info(String.format("Searching key '%s'", key));
		return redisTemplate.boundValueOps(key).get();
	}

	public Boolean expireAt(String key, Date dateTime) {
		return redisTemplate.expireAt(key, dateTime);
	}

	public Boolean setAndExpireAt(String key, String json, Date dateTime) {
		redisTemplate.opsForValue().set(key, json);
		return redisTemplate.expireAt(key, dateTime);
	}

	public void flushDb() {
		logger.info("Flushing all keys from current database");
		redisTemplate.getConnectionFactory().getConnection().flushDb();
	}

	public void flushAll() {
		logger.info("Flushing all keys from all databases");
		redisTemplate.getConnectionFactory().getConnection().flushAll();
	}
}
