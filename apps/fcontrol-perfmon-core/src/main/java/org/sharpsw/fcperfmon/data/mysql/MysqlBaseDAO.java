package org.sharpsw.fcperfmon.data.mysql;

import java.util.Collection;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

public abstract class MysqlBaseDAO<T extends Persistable, K> extends JdbcDaoSupport {
    protected Logger logger;

    public MysqlBaseDAO(Class<?> daoClass, DataSource dataSource) {
        logger = Logger.getLogger(daoClass);
        setDataSource(dataSource);
    }

    public abstract void save(T entity);

    public abstract void bulkSave(Collection<T> items);

    public abstract T findById(K key);

    public abstract Collection<T> findAll();
    
    public abstract void update(T entity);
    
    public abstract void delete(T entity);
}
