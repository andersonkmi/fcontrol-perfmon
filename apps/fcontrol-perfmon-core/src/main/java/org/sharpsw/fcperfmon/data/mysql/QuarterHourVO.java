package org.sharpsw.fcperfmon.data.mysql;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.sharpsw.fcperfmon.caching.Cacheable;

@Cacheable(isDisposable = false)
public class QuarterHourVO implements Persistable {
    private Integer id;
    private Calendar checkpoint;

    private QuarterHourVO() {}
    
    public static QuarterHourVO getInstance() {
    	QuarterHourVO instance = new QuarterHourVO();
    	instance.id = 0;
    	instance.checkpoint = Calendar.getInstance();
    	return instance;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setCheckpoint(Calendar time) {
        checkpoint = time;
    }

    public Calendar getCheckpoint() {
        return checkpoint;
    }

	@Override
	public String cacheId() {
        SimpleDateFormat formatter = new SimpleDateFormat("HH:mm");
        String hour = String.format("%s:00", formatter.format(checkpoint.getTime()));
		return String.format("QuarterHourVO_%s", hour);
	}
}