package org.sharpsw.fcperfmon.data.mssql;

import static java.math.RoundingMode.HALF_DOWN;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class FControlRequestRecordRowMapper implements RowMapper<FControlRequestRecordVO> {

    public FControlRequestRecordVO mapRow(ResultSet rs, int rowNum) throws SQLException {
        FControlRequestRecordVO record = FControlRequestRecordVO.getInstance();

        Integer quantity = rs.getInt("quantity");
        record.setQuantity(quantity);

        BigDecimal amount = rs.getBigDecimal("amount").setScale(2, HALF_DOWN);
        record.setAmount(amount);
        return record;
    }
}
