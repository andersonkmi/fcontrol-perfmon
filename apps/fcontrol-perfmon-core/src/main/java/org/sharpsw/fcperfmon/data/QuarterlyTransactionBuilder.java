package org.sharpsw.fcperfmon.data;

import org.sharpsw.fcperfmon.data.mssql.FControlRequestRecordVO;
import org.sharpsw.fcperfmon.data.mysql.CalendarVO;
import org.sharpsw.fcperfmon.data.mysql.QuarterHourVO;
import org.sharpsw.fcperfmon.data.mysql.QuarterlyTransactionVO;
import org.springframework.stereotype.Component;

@Component
public class QuarterlyTransactionBuilder {
	public QuarterlyTransactionVO build(FControlRequestRecordVO rawData, CalendarVO date, QuarterHourVO hour) {
		QuarterlyTransactionVO transaction = new QuarterlyTransactionVO();
		transaction.setUser(rawData.getUserId());
		transaction.setCalendar(date.getId());
		transaction.setQuarterHour(hour.getId());
		transaction.setAmount(rawData.getAmount());
		transaction.setQuantity(rawData.getQuantity());
		return transaction;
	}
}
