package org.sharpsw.fcperfmon.data.mysql;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class QuarterlyTransactionRowMapper implements RowMapper<QuarterlyTransactionVO> {

    @Override
    public QuarterlyTransactionVO mapRow(ResultSet rs, int rowNum) throws SQLException {
        QuarterlyTransactionVO element = new QuarterlyTransactionVO();
        element.setId(rs.getLong("id"));
        element.setUser(rs.getLong("user_id"));
        element.setCalendar(rs.getLong("calendar_id"));
        element.setQuarterHour(rs.getInt("quarter_hour_checkpoint_id"));
        element.setQuantity(rs.getInt("quantity"));
        element.setAmount(rs.getBigDecimal("amount"));
        return element;
    }

}
