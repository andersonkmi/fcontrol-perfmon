package org.sharpsw.fcperfmon.services;

import static java.math.BigDecimal.ROUND_HALF_DOWN;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;


import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.sharpsw.fcperfmon.data.mysql.CalendarDAO;
import org.sharpsw.fcperfmon.data.mysql.CalendarVO;
import org.sharpsw.fcperfmon.data.mysql.QuarterHourDAO;
import org.sharpsw.fcperfmon.data.mysql.QuarterHourVO;
import org.sharpsw.fcperfmon.data.mysql.QuarterlyTransactionDAO;
import org.sharpsw.fcperfmon.data.mysql.QuarterlyTransactionVO;
import org.sharpsw.fcperfmon.data.mysql.UserDAO;
import org.sharpsw.fcperfmon.data.mysql.UserVO;
import org.sharpsw.fcperfmon.graph.QuarterHourGraphData;
import org.sharpsw.fcperfmon.graph.QuarterHourQuantityGraphDataSetAmount;
import org.sharpsw.fcperfmon.graph.QuarterHourQuantityGraphDataSetQuantity;
import org.springframework.stereotype.Service;

@Service
public class QuarterHourGraphDataService {
	private static final Logger logger = Logger.getLogger(QuarterHourGraphDataService.class);
	
    @Resource
    private QuarterHourService quarterHourService;

    @Resource
    private CalendarDAO calendarDAO;

    @Resource
    private QuarterHourDAO quarterHourDAO;

    @Resource
    private QuarterlyTransactionDAO quarterlyTransactionDAO;

    @Resource
    private UserDAO userDAO;


    public QuarterHourGraphData findTransactionsQuantity(Calendar reference) {
    	logger.info("Retrieving data for reporting purposes");
    	CalendarVO calendar = findCalendar(reference);
    	List<UserVO> users = findAllUsers();
    	List<QuarterHourVO> periods = findTimePeriods(reference);    	
        Map<UserVO, List<QuarterlyTransactionVO>> items = findTransactions(calendar, users, periods);
        
        QuarterHourGraphData graphData = QuarterHourGraphData.newInstance();
        configureDataLabels(graphData, periods);
        configureDataSetQuantity(graphData, items);
        return graphData;
    }
    
    public QuarterHourGraphData findTransactionsAmount(Calendar reference) {
    	logger.info("Retrieving data for reporting purposes");
    	CalendarVO calendar = findCalendar(reference);
    	List<UserVO> users = findAllUsers();
    	List<QuarterHourVO> periods = findTimePeriods(reference);    	
        Map<UserVO, List<QuarterlyTransactionVO>> items = findTransactions(calendar, users, periods);
        
        QuarterHourGraphData graphData = QuarterHourGraphData.newInstance();
        configureDataLabels(graphData, periods);
        configureDataSetAmount(graphData, items);
        return graphData;
    }
    
    public QuarterHourGraphData findTransactionsQuantityByUserId(Calendar reference, Long userId) {
    	logger.info("Retrieving data for store");
    	CalendarVO calendar = findCalendar(reference);
    	List<UserVO> users = new ArrayList<>();
    	users.add(userDAO.findById(userId));
    	List<QuarterHourVO> periods = findTimePeriods(reference);
    	
        Map<UserVO, List<QuarterlyTransactionVO>> items = findTransactions(calendar, users, periods);        
        QuarterHourGraphData graphData = QuarterHourGraphData.newInstance();
        configureDataLabels(graphData, periods);
        configureDataSetQuantity(graphData, items);
        return graphData;
    	
    }
    
    public QuarterHourGraphData findTransactionsAmountByUserId(Calendar reference, Long userId) {
    	logger.info("Retrieving data for store");
    	CalendarVO calendar = findCalendar(reference);
    	List<UserVO> users = new ArrayList<>();
    	users.add(userDAO.findById(userId));
    	List<QuarterHourVO> periods = findTimePeriods(reference);
    	
        Map<UserVO, List<QuarterlyTransactionVO>> items = findTransactions(calendar, users, periods);        
        QuarterHourGraphData graphData = QuarterHourGraphData.newInstance();
        configureDataLabels(graphData, periods);
        configureDataSetAmount(graphData, items);
        return graphData;
    	
    }    

    private CalendarVO findCalendar(Calendar reference) {
    	logger.info("Finding date reference inside database");
        return calendarDAO.find(reference);
    }

    private List<UserVO> findAllUsers() {
    	logger.info("Finding all users from the database");
        return (List<UserVO>) userDAO.findAllEnabledMonitor();
    }

    private List<QuarterHourVO> findTimePeriods(Calendar reference) {
    	logger.info("Finding all periods from the database");
        int numberOfPreviousHours = 4;
        List<Calendar> items = quarterHourService.obtainPreviousQuarters(reference, numberOfPreviousHours);
        List<QuarterHourVO> results = new ArrayList<>();
        for(Calendar item : items) {
            QuarterHourVO element = quarterHourDAO.find(item);
            results.add(element);
        }
        return results;
    }

    private Map<UserVO, List<QuarterlyTransactionVO>> findTransactions(CalendarVO reference, List<UserVO> users, List<QuarterHourVO> periods) {
    	logger.info("Searching for all transactions in the data repository");
        Map<UserVO, List<QuarterlyTransactionVO>> results = new HashMap<>();

        for(UserVO user: users) {
        	results.put(user, new ArrayList<>());
            for(QuarterHourVO period : periods) {
            	QuarterlyTransactionVO element = quarterlyTransactionDAO.findByUserAndDate(user, reference, period);
                results.get(user).add(element);
            }
        }
        return results;
    }
    
    private void configureDataLabels(QuarterHourGraphData data, List<QuarterHourVO> periods) {
    	logger.info("Configuring the graph labels");
    	SimpleDateFormat formatter = new SimpleDateFormat("HH:mm");
    	for(QuarterHourVO period : periods) {
    		data.addLabel(formatter.format(period.getCheckpoint().getTime()));
    	}
    }
    
    private void configureDataSetQuantity(QuarterHourGraphData data, Map<UserVO, List<QuarterlyTransactionVO>> elements) {
    	logger.info("Configuring data sets");
    	Set<UserVO> keys = elements.keySet();
    	for(UserVO key : keys) {
    		QuarterHourQuantityGraphDataSetQuantity dataSet = QuarterHourQuantityGraphDataSetQuantity.getInstance();
    		dataSet.setLabel(key.getName());
    		
    		dataSet.setFillColor(key.getFillColor());
    		dataSet.setHighlightFill(key.getHighlightFill());
    		dataSet.setHighlightStroke(key.getHighlightStroke());
    		dataSet.setPointColor(key.getHighlightStroke());
    		dataSet.setStrokeColor(key.getHighlightStroke());
    		
    		List<QuarterlyTransactionVO> records = elements.get(key);
    		
    		for(QuarterlyTransactionVO record : records) {
    			dataSet.addData(record.getQuantity());
    		}
    		data.addDataSet(dataSet);
    	}
    }
    
    private void configureDataSetAmount(QuarterHourGraphData data, Map<UserVO, List<QuarterlyTransactionVO>> elements) {
    	logger.info("Configuring data sets");
    	Set<UserVO> keys = elements.keySet();
    	for(UserVO key : keys) {
    		QuarterHourQuantityGraphDataSetAmount dataSet = QuarterHourQuantityGraphDataSetAmount.getInstance();
    		dataSet.setLabel(key.getName());
    		
    		dataSet.setFillColor(key.getFillColor());
    		dataSet.setHighlightFill(key.getHighlightFill());
    		dataSet.setHighlightStroke(key.getHighlightStroke());
    		dataSet.setPointColor(key.getHighlightStroke());
    		dataSet.setStrokeColor(key.getHighlightStroke());
    		
    		List<QuarterlyTransactionVO> records = elements.get(key);
    		
    		for(QuarterlyTransactionVO record : records) {
    			dataSet.addData(record.getAmount().setScale(2, ROUND_HALF_DOWN));
    		}
    		data.addDataSet(dataSet);
    	}
    }
}
