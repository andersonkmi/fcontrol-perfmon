package org.sharpsw.fcperfmon.graph;

import java.math.BigDecimal;

public class QuarterHourQuantityGraphDataSetAmount  extends QuarterHourQuantityDataSet<BigDecimal> {

	public static QuarterHourQuantityGraphDataSetAmount getInstance() {
		return new QuarterHourQuantityGraphDataSetAmount();
	}
	
	private QuarterHourQuantityGraphDataSetAmount(){
		super();
	}	
	
}
