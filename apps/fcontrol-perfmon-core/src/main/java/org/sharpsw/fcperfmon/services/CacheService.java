package org.sharpsw.fcperfmon.services;

import static java.util.Calendar.HOUR;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.sharpsw.fcperfmon.caching.Cacheable;
import org.sharpsw.fcperfmon.caching.RedisCaching;
import org.sharpsw.fcperfmon.data.mysql.Persistable;
import org.sharpsw.fcperfmon.utils.JsonUtil;
import org.springframework.data.redis.RedisConnectionFailureException;
import org.springframework.stereotype.Service;

@Service
public class CacheService<T extends Persistable> {
    private static final Logger logger = Logger.getLogger(CacheService.class);

    @Resource
    private RedisCaching redisCaching;

    @Resource
    private JsonUtil<T> jsonUtil;

    public void put(T data) {
        if(!isCacheable(data)) {
            return;
        }

        String key = data.cacheId();
        String jsonRepresentation = jsonUtil.toJson(data);
        
        Cacheable cacheInfo = data.getClass().getAnnotation(Cacheable.class);
        int expirationInHours = cacheInfo.expireInHours();
        boolean isDisposable = cacheInfo.isDisposable();

        try {
            redisCaching.set(key, jsonRepresentation);            
            
            if(isDisposable) {
            	Calendar expirationDate = calculateExpiration(expirationInHours);
            	configureExpiration(key, expirationDate);

            	SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");        	
            	logger.info(String.format("Created cache key '%s' for record '%s' to be expired on '%s'", key, jsonRepresentation, sdf.format(expirationDate.getTime())));    	
            } else {
            	logger.info(String.format("Created cache key '%s' for record '%s' with no expiration'", key, jsonRepresentation));
            }
            
        } catch (RedisConnectionFailureException exception) {
            logger.warn(String.format("Redis caching failed for record: '%s' - '%s'", key, jsonRepresentation), exception);
        }
    }

    public T get(String key, Class<T> cls) {
        logger.info(String.format("Searching for key = '%s'", key));
        T result = null;
        String jsonValue = redisCaching.get(key);
        if(jsonValue != null) {
            result = jsonUtil.fromJson(jsonValue, cls);
        }
        return result;
    }
    
    private Calendar calculateExpiration(int expirationInHours) {
    	Calendar expirationDate = Calendar.getInstance();
    	expirationDate.add(HOUR, expirationInHours);
    	return expirationDate;
    }
    
    private void configureExpiration(String key, Calendar expirationDate) {
    	logger.info(String.format("Cache record '%s' is disposable", key));
    	redisCaching.expireAt(key, expirationDate.getTime());
    }

    private boolean isCacheable(T data) {
        Cacheable cacheInfo = data.getClass().getAnnotation(Cacheable.class);
        return cacheInfo != null;
    }
}
