package org.sharpsw.fcperfmon.graph;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class QuarterHourGraphData {
	
	private List<String> labels;
	@SuppressWarnings("rawtypes")
	private List<QuarterHourQuantityDataSet> datasets;
	
	public static QuarterHourGraphData newInstance() {
		return new QuarterHourGraphData();
	}
	
	public void addLabel(String label) {
		labels.add(label);
	}
	
	@SuppressWarnings("rawtypes")
	public void addDataSet(QuarterHourQuantityDataSet data) {
		datasets.add(data);
	}
	
	public List<String> getLabels() {
		return Collections.unmodifiableList(labels);
	}
	
	@SuppressWarnings("rawtypes")
	public List<QuarterHourQuantityDataSet> getDatasets() {
		return Collections.unmodifiableList(datasets);
	}

	private QuarterHourGraphData() {
		labels = new LinkedList<>();
		datasets = new LinkedList<>();
	}
}
