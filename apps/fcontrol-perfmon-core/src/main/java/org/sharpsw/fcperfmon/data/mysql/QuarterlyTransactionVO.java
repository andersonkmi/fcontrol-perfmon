package org.sharpsw.fcperfmon.data.mysql;

import org.sharpsw.fcperfmon.caching.Cacheable;
import java.math.BigDecimal;

import static org.sharpsw.fcperfmon.services.MonitoringServiceType.QUARTERLY_HOUR_MONITOR;

@Cacheable(expireInHours = 720)
public class QuarterlyTransactionVO implements Persistable {
    private Long id;
    private Long userId;
    private Long calendarId;
    private Integer quarterHourId;
    private Integer quantity;
    private BigDecimal amount;

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setUser(Long user) {
        userId = user;
    }

    public Long getUser() {
        return userId;
    }

    public void setCalendar(Long calendar) {
        calendarId = calendar;
    }

    public Long getCalendar() {
        return calendarId;
    }

    public void setQuarterHour(Integer quarterHour) {
        quarterHourId = quarterHour;
    }

    public Integer getQuarterHour() {
        return quarterHourId;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    @Override
    public String cacheId() {
        return String.format("%s_%s_%s_%s",
                                            QUARTERLY_HOUR_MONITOR.getName(),
                                            userId.toString(),
                                            calendarId.toString(),
                                            quarterHourId.toString());
    }
}
