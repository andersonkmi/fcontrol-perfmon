package org.sharpsw.fcperfmon.data.mssql;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

public class MSSQLBaseDAO extends JdbcDaoSupport {
	protected Logger logger;
	
	public MSSQLBaseDAO(Class<?> daoClass, DataSource dataSource) {
		logger = Logger.getLogger(daoClass);
		setDataSource(dataSource);
	}
}
