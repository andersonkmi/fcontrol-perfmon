package org.sharpsw.fcperfmon.data.mysql;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.sharpsw.fcperfmon.services.CacheService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class QuarterHourDAO extends MysqlBaseDAO<QuarterHourVO, Integer> {
	@Resource
	private CacheService<QuarterHourVO> cacheService;
	
	@Autowired
	public QuarterHourDAO(DataSource mysqlDataSource) {
		super(QuarterHourDAO.class, mysqlDataSource);
	}

	@Override
	@Transactional("mysqlTxManager")
	public void save(QuarterHourVO entity) {
		String statement = "INSERT INTO quarter_hour_checkpoint (id, checkpoint) VALUES (?, ?)";
		getJdbcTemplate().update(statement, new Object[] {entity.getId(), entity.getCheckpoint().getTime()});
		cacheService.put(entity);
	}

	@Override
	@Transactional("mysqlTxManager")
	public void bulkSave(Collection<QuarterHourVO> items) {
		items.forEach(this::save);
	}

	@Override
	@Transactional("mysqlTxManager")
	public QuarterHourVO findById(Integer key) {
		String statement = "SELECT id, checkpoint FROM quarter_hour_checkpoint WHERE id = ?";
		QuarterHourVO item = getJdbcTemplate().queryForObject(statement, new QuarterHourRowMapper(), new Object[] {key});
		return item;
	}

	@Override
	@Transactional("mysqlTxManager")
	public Collection<QuarterHourVO> findAll() {
		String statement = "SELECT id, checkpoint FROM quarter_hour_checkpoint";
		Collection<QuarterHourVO> elements = getJdbcTemplate().query(statement, new BeanPropertyRowMapper<QuarterHourVO>());
		return elements;
	}

	@Override
	@Transactional("mysqlTxManager")
	public void update(QuarterHourVO entity) {
	}

	@Override
	@Transactional("mysqlTxManager")
	public void delete(QuarterHourVO entity) {
	}

	@Transactional("mysqlTxManager")
	public QuarterHourVO find(Calendar dateTime) {
		String statement = "SELECT id, checkpoint FROM quarter_hour_checkpoint WHERE checkpoint = ?";		
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
		String formattedTime = String.format("%s:00", sdf.format(dateTime.getTime()));

		QuarterHourVO item = cacheService.get(String.format("QuarterHourVO_%s", formattedTime), QuarterHourVO.class);
		if(item != null) {
			return item;
		}

		logger.info(String.format("Finding record for hour: '%s'", formattedTime));		
		item = getJdbcTemplate().queryForObject(statement, new QuarterHourRowMapper(), new Object[] { formattedTime });
		cacheService.put(item);
		return item;
	}
}
