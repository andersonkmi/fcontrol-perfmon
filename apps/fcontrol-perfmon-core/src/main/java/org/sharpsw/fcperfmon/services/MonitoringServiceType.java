package org.sharpsw.fcperfmon.services;

public enum MonitoringServiceType {

    QUARTERLY_HOUR_MONITOR("QuarterlyHour"),
    HOURLY_MONITOR("HourlyMonitor");

    private String name;

    MonitoringServiceType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
