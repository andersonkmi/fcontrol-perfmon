package org.sharpsw.fcperfmon.data.mssql;

import static java.math.BigDecimal.ZERO;
import static java.math.RoundingMode.HALF_DOWN;

import java.math.BigDecimal;

public class FControlRequestRecordVO {
    private Long userId = new Long(0);
    private Integer quantity = 0;
    private BigDecimal amount = ZERO.setScale(2, HALF_DOWN);

    private FControlRequestRecordVO() {}
    
    public static FControlRequestRecordVO getInstance() {
    	return new FControlRequestRecordVO();    		
    }
    
    public void setUserId(Long id) {
        userId = id;
    }

    public Long getUserId() {
        return userId;
    }
        
    public void setQuantity(Integer quantity) {
    	this.quantity = quantity;
    }
    
    public Integer getQuantity() {
    	return quantity;
    }
    
    public void setAmount(BigDecimal amount) {
    	this.amount = amount;
    }
    
    public BigDecimal getAmount() {
    	return amount;
    }
}
