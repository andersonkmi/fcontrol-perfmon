package org.sharpsw.fcperfmon.services;

import static java.util.Calendar.MILLISECOND;
import static java.util.Calendar.MINUTE;
import static java.util.Calendar.SECOND;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.sharpsw.fcperfmon.data.QuarterlyTransactionBuilder;
import org.sharpsw.fcperfmon.data.mssql.FControlRequestRecordVO;
import org.sharpsw.fcperfmon.data.mssql.QuarterHourTransactionDAO;
import org.sharpsw.fcperfmon.data.mysql.CalendarDAO;
import org.sharpsw.fcperfmon.data.mysql.CalendarVO;
import org.sharpsw.fcperfmon.data.mysql.QuarterHourDAO;
import org.sharpsw.fcperfmon.data.mysql.QuarterHourVO;
import org.sharpsw.fcperfmon.data.mysql.QuarterlyTransactionDAO;
import org.sharpsw.fcperfmon.data.mysql.QuarterlyTransactionVO;
import org.sharpsw.fcperfmon.data.mysql.UserDAO;
import org.sharpsw.fcperfmon.data.mysql.UserVO;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

@Service
public class QuarterlyHourMonitoringService implements MonitoringService {
    private static final Logger logger = Logger.getLogger(QuarterlyHourMonitoringService.class);

    @Resource
    private UserDAO userDAO;

    @Resource
    private CalendarDAO calendarDAO;

    @Resource
    private QuarterHourDAO quarterHourDAO;

    @Resource
    private QuarterHourTransactionDAO quarterHourTransactionDAO;

    @Resource
    private QuarterlyTransactionBuilder quarterlyTransactionBuilder;
    
    @Resource
    private QuarterlyTransactionDAO quarterlyTransactionDAO;

    @Override
    public void executeMonitoring() {
        logger.info("Starting quarter hour monitoring service.");

    	Calendar now = Calendar.getInstance();
    	resetCurrentTime(now);    	    	
    	CalendarVO date = calendarDAO.find(now);
    	QuarterHourVO hour = quarterHourDAO.find(now);    	
    	Calendar startPeriod = createStartDateTime(now);

    	List<UserVO> users = (List<UserVO>) userDAO.findAllEnabledMonitor();

		if(!users.isEmpty()) {
			List<FControlRequestRecordVO> items = quarterHourTransactionDAO.calculate(users, startPeriod, now);
			List<QuarterlyTransactionVO> results = generateRecords(items, date, hour);
			saveRecords(results);
		} else {
			logger.info("No user configured to be monitored");
		}
        logger.info("Finished quarter hour monitoring service.");
    }
    
    private void resetCurrentTime(Calendar dateTime) {
    	dateTime.set(SECOND, 0);
    	dateTime.set(MILLISECOND, 0);
    }
    
    private Calendar createStartDateTime(Calendar reference) {
    	Calendar startDate = Calendar.getInstance();
    	startDate.setTime(reference.getTime());
    	startDate.add(MINUTE, -15);
    	startDate.set(SECOND, 1);
    	return startDate;
    }
    
    private List<QuarterlyTransactionVO> generateRecords(List<FControlRequestRecordVO> items, CalendarVO calendar, QuarterHourVO hour) {
    	logger.info("Generating records to be persisted into the database");
    	List<QuarterlyTransactionVO> results = new ArrayList<>();
    	for(FControlRequestRecordVO item : items) {
    		QuarterlyTransactionVO element = quarterlyTransactionBuilder.build(item, calendar, hour);
    		results.add(element);
    	}
    	return results;
    }
    
    private void saveRecords(Collection<QuarterlyTransactionVO> items) {
        logger.info("Saving records into the database.");
        try {
        	quarterlyTransactionDAO.bulkSave(items);        	
        } catch(DataAccessException exception) {
        	logger.fatal("Exception raised when saving the records into the database", exception);
        }
    }
}
