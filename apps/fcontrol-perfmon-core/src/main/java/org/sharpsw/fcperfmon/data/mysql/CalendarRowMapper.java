package org.sharpsw.fcperfmon.data.mysql;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;

import org.springframework.jdbc.core.RowMapper;

public class CalendarRowMapper implements RowMapper<CalendarVO> {

	@Override
	public CalendarVO mapRow(ResultSet rs, int rowNum) throws SQLException {
		CalendarVO calendar = CalendarVO.getInstance();
		calendar.setId(rs.getLong("id"));
		
		Calendar date = Calendar.getInstance();
		date.setTime(rs.getDate("calendar_date"));		
		calendar.setCalendarDate(date);
		
		return calendar;
	}

}
