package org.sharpsw.fcperfmon.data.mysql;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class UserRowMapper implements RowMapper<UserVO> {

	@Override
	public UserVO mapRow(ResultSet rs, int rowNum) throws SQLException {
		UserVO user = UserVO.getInstance();
		user.setId(rs.getLong("id"));
		user.setName(rs.getString("name"));
		user.setIsDeviceFingerprintInUse(rs.getBoolean("device_fingerprint_enabled"));
		user.setIsReplacement(rs.getBoolean("is_exchange"));
		user.setIsMaster(rs.getBoolean("is_master"));
		user.setIsMonitoringEnabled(rs.getBoolean("is_monitoring_enabled"));
		user.setFillColor(String.format("rgba(%s)", rs.getString("fillColor_RGBA")));
		user.setHighlightFill(String.format("rgba(%s)", rs.getString("highlightFill_RGBA")));
		user.setHighlightStroke(String.format("rgba(%s)", rs.getString("highlightStroke_RGBA")));
		return user;
	}
}
