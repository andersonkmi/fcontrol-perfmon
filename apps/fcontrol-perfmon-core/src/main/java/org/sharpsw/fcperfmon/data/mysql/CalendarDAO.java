package org.sharpsw.fcperfmon.data.mysql;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.sharpsw.fcperfmon.services.CacheService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class CalendarDAO extends MysqlBaseDAO<CalendarVO, Long> {
	@Resource
	private CacheService<CalendarVO> cacheService;
	
	@Autowired
	public CalendarDAO(DataSource mysqlDataSource) {
		super(CalendarDAO.class, mysqlDataSource);
	}
	
	@Override
	@Transactional("mysqlTxManager")
	public void save(CalendarVO entity) {
	}

	@Override
	@Transactional("mysqlTxManager")
	public void bulkSave(Collection<CalendarVO> items) {
		
	}

	@Override
	@Transactional("mysqlTxManager")
	public CalendarVO findById(Long key) {
		String statement = "SELECT id, calendar_date FROM calendar WHERE id = ?";
		CalendarVO result = getJdbcTemplate().queryForObject(statement, new CalendarRowMapper(), new Object[] {key});
		return result;
	}

	@Override
	@Transactional("mysqlTxManager")
	public Collection<CalendarVO> findAll() {
		String statement = "SELECT id, calendar_date FROM calendar";
		Collection<CalendarVO> results = getJdbcTemplate().query(statement, new CalendarRowMapper());
		return results;
	}

	@Transactional("mysqlTxManager")
	public CalendarVO find(Calendar date) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String formattedDate = sdf.format(date.getTime());
		logger.info(String.format("Searching the date '%s'", formattedDate));

		String key = String.format("CalendarVO_%s", formattedDate);
		CalendarVO result = cacheService.get(key, CalendarVO.class);
		if(result != null) {
			logger.info("Calendar found in cache");
			return result;
		}
		logger.info("Calendar not found in cache");

		String statement = "SELECT id, calendar_date FROM calendar WHERE calendar_date = ?";
		try {
			Date parsedDate = sdf.parse(formattedDate);
			result = getJdbcTemplate().queryForObject(statement, new CalendarRowMapper(), new Object[] { parsedDate });
			cacheService.put(result);
		} catch (ParseException exception) {
			logger.error(String.format("Date parse exception: '%s'", exception.getMessage()), exception);
		}
		return result;
	}

	@Override
	@Transactional("mysqlTxManager")
	public void update(CalendarVO entity) {
	}

	@Override
	@Transactional("mysqlTxManager")
	public void delete(CalendarVO entity) {
	}

}
