package org.sharpsw.fcperfmon.data.mysql;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.sharpsw.fcperfmon.caching.Cacheable;

@Cacheable(isDisposable = false)
public class CalendarVO implements Persistable {
    private Long id;
    private Calendar calendarDate;

    private CalendarVO() {}
    
    public static CalendarVO getInstance() {
    	CalendarVO instance = new CalendarVO();
    	instance.id = new Long(0);
    	instance.calendarDate = Calendar.getInstance();
    	return instance;
    }
    
    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setCalendarDate(Calendar date) {
        calendarDate = date;
    }

    public Calendar getCalendarDate() {
        return calendarDate;
    }

	@Override
	public String cacheId() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		return String.format("CalendarVO_%s", sdf.format(calendarDate.getTime()));
	}
}
