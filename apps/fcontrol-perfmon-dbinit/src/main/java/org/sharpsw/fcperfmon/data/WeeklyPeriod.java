package org.sharpsw.fcperfmon.data;

import java.util.Calendar;

public class WeeklyPeriod {
	private Long id;
	private Calendar startDate;
	private Calendar endDate;
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public Long getId() {
		return id;
	}
	
	public void setStartDate(Calendar date) {
		startDate = date;
	}
	
	public Calendar getStartDate() {
		return startDate;
	}
	
	public void setEndDate(Calendar date) {
		endDate = date;
	}
	
	public Calendar getEndDate() {
		return endDate;
	}
}
