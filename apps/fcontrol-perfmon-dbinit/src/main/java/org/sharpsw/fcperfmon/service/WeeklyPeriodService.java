package org.sharpsw.fcperfmon.service;

import static org.joda.time.DateTimeConstants.MONDAY;
import static org.joda.time.DateTimeConstants.SUNDAY;

import java.util.LinkedList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joda.time.DateTime;
import org.sharpsw.fcperfmon.data.WeeklyPeriod;
import org.sharpsw.fcperfmon.data.WeeklyPeriodDAO;
import org.springframework.stereotype.Service;

@Service
public class WeeklyPeriodService {
	private static final Logger logger = LogManager.getLogger(CalendarService.class);
	private static final int WEEKLY_INCREMENT = 156;
	
	@Resource
	private WeeklyPeriodDAO weeklyPeriodDAO;
	
	public void configure() {
		List<WeeklyPeriod> items = new LinkedList<>();
		logger.info("Configuring the weekly_checkpoint table");
		if(weeklyPeriodDAO.isEmpty()) {
			initialize(items);
		} else {
			increment(items);
		}
		weeklyPeriodDAO.bulkSave(items);
	}
	
	private void initialize(List<WeeklyPeriod> items) {
		logger.info("Initializing weekly_checkpoint database");
		DateTime currentDateTime = DateTime.now();
		for(int index = 0; index < WEEKLY_INCREMENT; index++) {
			DateTime reference = currentDateTime.plusWeeks(index);
			DateTime weekStartDate = getWeekStartDate(reference);
			DateTime weekEndDate = getWeekEndDate(weekStartDate);
			
			WeeklyPeriod period = new WeeklyPeriod();
			period.setStartDate(weekStartDate.toGregorianCalendar());
			period.setEndDate(weekEndDate.toGregorianCalendar());
			
			items.add(period);
		}
		
	}
	
	private void increment(List<WeeklyPeriod> items) {
		WeeklyPeriod latest = weeklyPeriodDAO.findLatest();
		if(latest != null) {
			DateTime latestStartDate = new DateTime(latest.getStartDate().getTimeInMillis());
			for(int index = 1; index <= WEEKLY_INCREMENT; index++) {
				DateTime reference = latestStartDate.plusWeeks(index);
				DateTime weekStartDate = getWeekStartDate(reference);
				DateTime weekEndDate = getWeekEndDate(weekStartDate);
				
				WeeklyPeriod period = new WeeklyPeriod();
				period.setStartDate(weekStartDate.toGregorianCalendar());
				period.setEndDate(weekEndDate.toGregorianCalendar());
				
				items.add(period);
			}
		}
	}
	
	private DateTime getWeekStartDate(DateTime date) {		
		if(date.getDayOfWeek() == MONDAY) {			
			return date;
		} else {
			DateTime mondayDateTime = date.withDayOfWeek(MONDAY);
			return mondayDateTime;
		}		
	}
	
	private DateTime getWeekEndDate(DateTime date) {
		DateTime sundayDateTime = date.withDayOfWeek(SUNDAY);
		return sundayDateTime;
	}
}
