package org.sharpsw.fcperfmon.data;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class WeeklyPeriodDAO extends BaseDAO {
		
	@Autowired
	public WeeklyPeriodDAO(DataSource fcontrolPerfmonDataSource) {
		super(WeeklyPeriodDAO.class);
		setDataSource(fcontrolPerfmonDataSource);
	}
	
	@Transactional
	public boolean isEmpty() {
		logger.info("Verifying if the 'weekly_checkpoint' table is empty ");
		String sql = "SELECT COUNT(1) as quantity FROM weekly_checkpoint";		
		Integer quantity = getJdbcTemplate().queryForObject(sql, Integer.class);
		
		if(quantity > 0) {
			return false;
		} else {
			return true;
		}
	}
	
	@Transactional
	public void save(Calendar startDate, Calendar endDate) {
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		logger.info(String.format("Saving a new set of weekly period using the following dates: '%s' - '%s'", sdf.format(startDate.getTime()), sdf.format(endDate.getTime())));
		String statement = "INSERT INTO weekly_checkpoint(starting_date, ending_date) VALUES (?, ?)";
		getJdbcTemplate().update(statement, new Object[] { startDate.getTime(), endDate.getTime() });
	}
	
	@Transactional
	public void bulkSave(List<WeeklyPeriod> dates) {
		logger.info("Invoking bulk save");
		for(WeeklyPeriod date : dates) {
			save(date.getStartDate(), date.getEndDate());
		}
	}
	
	@Transactional
	public WeeklyPeriod findLatest() {
		String statement = "SELECT id, starting_date, ending_date FROM weekly_checkpoint ORDER BY id DESC LIMIT 1";
		
		WeeklyPeriod period = getJdbcTemplate().queryForObject(statement, new WeeklyPeriodRowMapper());
		return period;
	}
}
