package org.sharpsw.fcperfmon.data;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;

import org.springframework.jdbc.core.RowMapper;

public class WeeklyPeriodRowMapper implements RowMapper<WeeklyPeriod> {
	
	public WeeklyPeriod mapRow(ResultSet rs, int rowNum) throws SQLException {
		WeeklyPeriod period = new WeeklyPeriod();
		Long id = rs.getLong("id");
		
		Calendar startDate = Calendar.getInstance();			
		startDate.setTimeInMillis(rs.getDate("starting_date").getTime());
		
		Calendar endDate = Calendar.getInstance();			
		endDate.setTimeInMillis(rs.getDate("ending_date").getTime());

		period.setId(id);
		period.setStartDate(startDate);
		period.setEndDate(endDate);
		return period;
	}
}
