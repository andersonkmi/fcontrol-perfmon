package org.sharpsw.fcperfmon.service;

import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joda.time.DateTime;
import org.sharpsw.fcperfmon.data.CalendarDAO;
import org.springframework.stereotype.Service;

@Service
public class CalendarService {
	private static final Logger logger = LogManager.getLogger(CalendarService.class);
	private static final int INCREMENT_IN_DAYS = 1580;
	@Resource
	private CalendarDAO calendarDAO;
	
	public void configure() {
		List<Calendar> dates = new LinkedList<>();
		if(calendarDAO.isTableEmpty()) {
			initialize(dates);
		} else {
			increment(dates);
		}
		calendarDAO.bulkSpave(dates);
	}
	
	private void initialize(List<Calendar> dates) {
		logger.info("Initializing the calendar table");
		for(int index = 0; index < INCREMENT_IN_DAYS; index++) {				
			logger.info(String.format("Adding '%s' to be inserted", DateTime.now().plusDays(index)));
			dates.add(DateTime.now().plusDays(index).toGregorianCalendar());
		}		
	}
	
	private void increment(List<Calendar> dates) {
		logger.info("Increasing the calendar table");		
		Calendar latest = calendarDAO.findLatest();
		DateTime latestDateTime = new DateTime(latest.getTimeInMillis());
		for(int index = 1; index <= INCREMENT_IN_DAYS; index++) {
			logger.info(String.format("Adding '%s' date", latestDateTime.plusDays(index)));
			dates.add(latestDateTime.plusDays(index).toGregorianCalendar());
		}
	}
}
