package org.sharpsw.fcperfmon.data;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Collection;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class MonthlyCheckpointDAO extends BaseDAO {

	@Autowired
	public MonthlyCheckpointDAO(DataSource fcontrolPerfmonDataSource) {
		super(MonthlyCheckpointDAO.class);
		setDataSource(fcontrolPerfmonDataSource);
	}
	
	@Transactional
	public boolean isEmpty() {
		logger.info("Verifying if the 'monthly_checkpoint' table is empty ");
		String sql = "SELECT COUNT(1) as quantity FROM monthly_checkpoint";		
		Integer quantity = getJdbcTemplate().queryForObject(sql, Integer.class);
		
		if(quantity > 0) {
			return false;
		} else {
			return true;
		}
	}
	
	@Transactional
	public Calendar findLatest() {
		logger.info("Retrieving the latest record in the table");
		
		String sql = "SELECT checkpoint_date FROM monthly_checkpoint ORDER BY id DESC LIMIT 1";		
		Timestamp record = getJdbcTemplate().queryForObject(sql, Timestamp.class);
		
		Calendar date = Calendar.getInstance();
		date.setTimeInMillis(record.getTime());
		return date;
	}
	
	@Transactional
	public void save(Calendar date) {
		String sql = "INSERT INTO monthly_checkpoint (checkpoint_date) VALUES (?)";
		getJdbcTemplate().update(sql, new Object[] { date.getTime() });
	}
	
	@Transactional
	public void bulkSpave(Collection<Calendar> dates) {
		for(Calendar date : dates) {
			save(date);
		}
	}
	
}
