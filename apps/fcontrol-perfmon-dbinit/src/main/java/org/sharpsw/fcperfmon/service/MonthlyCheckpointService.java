package org.sharpsw.fcperfmon.service;

import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joda.time.DateTime;
import org.sharpsw.fcperfmon.data.MonthlyCheckpointDAO;
import org.springframework.stereotype.Service;

@Service
public class MonthlyCheckpointService {
	private static final Logger logger = LogManager.getLogger(MonthlyCheckpointService.class);
	
	private static final int INCREMENT_IN_MONTHS = 36;
	@Resource
	private MonthlyCheckpointDAO monthlyCheckpointDAO;
	
	public void configure() {
		List<Calendar> items = new LinkedList<>();
		if(monthlyCheckpointDAO.isEmpty()) {
			initialize(items);
		} else {
			increment(items);
		}
		monthlyCheckpointDAO.bulkSpave(items);
	}
	
	private void initialize(List<Calendar> items) {
		logger.info("Initializing the monthly_checkpoint table");
		for(int index = 0; index < INCREMENT_IN_MONTHS; index++) {
			DateTime dt = DateTime.now().plusMonths(index).withDayOfMonth(1);			
			logger.info(String.format("Adding '%s' to be inserted", dt.toString()));
			items.add(dt.toGregorianCalendar());
		}				
	}
	
	private void increment(List<Calendar> dates) {
		logger.info("Increasing the calendar table");		
		Calendar latest = monthlyCheckpointDAO.findLatest();
		DateTime latestDateTime = new DateTime(latest.getTimeInMillis());
		for(int index = 1; index <= INCREMENT_IN_MONTHS; index++) {
			logger.info(String.format("Adding '%s' date", latestDateTime.plusDays(index)));
			dates.add(latestDateTime.plusDays(index).toGregorianCalendar());
		}
	}	
}
