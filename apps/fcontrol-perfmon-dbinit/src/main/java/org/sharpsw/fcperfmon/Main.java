package org.sharpsw.fcperfmon;

import org.sharpsw.fcperfmon.service.CalendarService;
import org.sharpsw.fcperfmon.service.MonthlyCheckpointService;
import org.sharpsw.fcperfmon.service.WeeklyPeriodService;
import org.springframework.context.support.ClassPathXmlApplicationContext;



public class Main {
	
	public static void main(String[] args) {
		String[] configs = { "classpath:applicationContext.xml" };

		ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext(configs);
		ctx.registerShutdownHook();

		CalendarService utility = (CalendarService) ctx.getBean("calendarService");
		utility.configure();
		
		WeeklyPeriodService weeklyPeriodService = (WeeklyPeriodService) ctx.getBean("weeklyPeriodService");
		weeklyPeriodService.configure();
		
		MonthlyCheckpointService monthlyCheckpointService = (MonthlyCheckpointService) ctx.getBean("monthlyCheckpointService");
		monthlyCheckpointService.configure();

		ctx.close();
	}

}
