package org.sharpsw.fcperfmon.data;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

public class BaseDAO extends JdbcDaoSupport {
	protected Logger logger;
	
	public BaseDAO(Class<?> daoClass) {
		this.logger = LogManager.getLogger(daoClass);
	}
}
