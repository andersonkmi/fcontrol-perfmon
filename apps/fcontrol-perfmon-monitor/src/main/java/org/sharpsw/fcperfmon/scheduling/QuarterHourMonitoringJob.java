package org.sharpsw.fcperfmon.scheduling;

import java.text.SimpleDateFormat;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.sharpsw.fcperfmon.services.MonitoringService;
import org.sharpsw.fcperfmon.services.QuarterlyHourMonitoringService;
import org.springframework.scheduling.quartz.QuartzJobBean;

public class QuarterHourMonitoringJob extends QuartzJobBean {
    private static final Logger logger = Logger.getLogger(QuarterlyHourMonitoringService.class);

    @Resource
    private MonitoringService service;

    @Override
    protected void executeInternal(JobExecutionContext ctx) throws JobExecutionException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        logger.info(String.format("Starting quarter hour monitoring: Fire date/time '%s'", sdf.format(ctx.getFireTime())));
        service.executeMonitoring();
    }

    public void setMonitoringService(MonitoringService service) {
        this.service = service;
    }
}
