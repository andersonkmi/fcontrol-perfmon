package org.sharpsw.fcperfmon.ws;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

import java.util.Calendar;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.sharpsw.fcperfmon.graph.QuarterHourGraphData;
import org.sharpsw.fcperfmon.services.QuarterHourGraphDataService;
import org.sharpsw.fcperfmon.utils.JsonUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/services")
public class QuarterHourMonitorWebService {
	private static final Logger logger = Logger.getLogger(QuarterHourMonitorWebService.class);
	
	@Resource
	private QuarterHourGraphDataService quarterHourGraphDataService;
	
	@Resource
	private JsonUtil<QuarterHourGraphData> jsonUtil;
	
	@RequestMapping(value = "/quarterHourTransactionQuantity", method = GET)
	public @ResponseBody String loadQuarterHourGraph() {
		logger.info("Calling the quarter hour graph data loaders");
		QuarterHourGraphData results = quarterHourGraphDataService.findTransactionsQuantity(Calendar.getInstance());
		String json = jsonUtil.toJson(results);
		return json;
	}	

	@RequestMapping(value = "/quarterHourTransactionAmount", method = GET)
	public @ResponseBody String loadQuarterHourGraphAmount() {
		logger.info("Calling the quarter hour graph data loaders");
		QuarterHourGraphData results = quarterHourGraphDataService.findTransactionsAmount(Calendar.getInstance());
		String json = jsonUtil.toJson(results);
		return json;
	}

	@RequestMapping(value = "/quarterHourTransactionQuantity/{userId}", method = GET)
	public @ResponseBody String loadQuarterHourGraphByUserId(@PathVariable Long userId) {
		logger.info("Calling the quarter hour graph data loaders");
		QuarterHourGraphData results = quarterHourGraphDataService.findTransactionsQuantityByUserId(Calendar.getInstance(), userId);
		String json = jsonUtil.toJson(results);
		return json;
	}	
	
	@RequestMapping(value = "/quarterHourTransactionAmount/{userId}", method = GET)
	public @ResponseBody String loadQuarterHourAmountGraphByUserId(@PathVariable Long userId) {
		logger.info("Calling the quarter hour graph data loaders");
		QuarterHourGraphData results = quarterHourGraphDataService.findTransactionsAmountByUserId(Calendar.getInstance(), userId);
		String json = jsonUtil.toJson(results);
		return json;
	}	
}
