CREATE TABLE calendar ( 
	id BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	calendar_date DATE NOT NULL UNIQUE 
);

CREATE TABLE quarter_hour_checkpoint ( 
	id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	checkpoint TIME NOT NULL UNIQUE 
);

INSERT INTO quarter_hour_checkpoint (checkpoint) VALUES ('00:00:00');
INSERT INTO quarter_hour_checkpoint (checkpoint) VALUES ('00:15:00');
INSERT INTO quarter_hour_checkpoint (checkpoint) VALUES ('00:30:00');
INSERT INTO quarter_hour_checkpoint (checkpoint) VALUES ('00:45:00');

INSERT INTO quarter_hour_checkpoint (checkpoint) VALUES ('01:00:00');
INSERT INTO quarter_hour_checkpoint (checkpoint) VALUES ('01:15:00');
INSERT INTO quarter_hour_checkpoint (checkpoint) VALUES ('01:30:00');
INSERT INTO quarter_hour_checkpoint (checkpoint) VALUES ('01:45:00');

INSERT INTO quarter_hour_checkpoint (checkpoint) VALUES ('02:00:00');
INSERT INTO quarter_hour_checkpoint (checkpoint) VALUES ('02:15:00');
INSERT INTO quarter_hour_checkpoint (checkpoint) VALUES ('02:30:00');
INSERT INTO quarter_hour_checkpoint (checkpoint) VALUES ('02:45:00');

INSERT INTO quarter_hour_checkpoint (checkpoint) VALUES ('03:00:00');
INSERT INTO quarter_hour_checkpoint (checkpoint) VALUES ('03:15:00');
INSERT INTO quarter_hour_checkpoint (checkpoint) VALUES ('03:30:00');
INSERT INTO quarter_hour_checkpoint (checkpoint) VALUES ('03:45:00');

INSERT INTO quarter_hour_checkpoint (checkpoint) VALUES ('04:00:00');
INSERT INTO quarter_hour_checkpoint (checkpoint) VALUES ('04:15:00');
INSERT INTO quarter_hour_checkpoint (checkpoint) VALUES ('04:30:00');
INSERT INTO quarter_hour_checkpoint (checkpoint) VALUES ('04:45:00');

INSERT INTO quarter_hour_checkpoint (checkpoint) VALUES ('05:00:00');
INSERT INTO quarter_hour_checkpoint (checkpoint) VALUES ('05:15:00');
INSERT INTO quarter_hour_checkpoint (checkpoint) VALUES ('05:30:00');
INSERT INTO quarter_hour_checkpoint (checkpoint) VALUES ('05:45:00');

INSERT INTO quarter_hour_checkpoint (checkpoint) VALUES ('06:00:00');
INSERT INTO quarter_hour_checkpoint (checkpoint) VALUES ('06:15:00');
INSERT INTO quarter_hour_checkpoint (checkpoint) VALUES ('06:30:00');
INSERT INTO quarter_hour_checkpoint (checkpoint) VALUES ('06:45:00');

INSERT INTO quarter_hour_checkpoint (checkpoint) VALUES ('07:00:00');
INSERT INTO quarter_hour_checkpoint (checkpoint) VALUES ('07:15:00');
INSERT INTO quarter_hour_checkpoint (checkpoint) VALUES ('07:30:00');
INSERT INTO quarter_hour_checkpoint (checkpoint) VALUES ('07:45:00');

INSERT INTO quarter_hour_checkpoint (checkpoint) VALUES ('08:00:00');
INSERT INTO quarter_hour_checkpoint (checkpoint) VALUES ('08:15:00');
INSERT INTO quarter_hour_checkpoint (checkpoint) VALUES ('08:30:00');
INSERT INTO quarter_hour_checkpoint (checkpoint) VALUES ('08:45:00');

INSERT INTO quarter_hour_checkpoint (checkpoint) VALUES ('09:00:00');
INSERT INTO quarter_hour_checkpoint (checkpoint) VALUES ('09:15:00');
INSERT INTO quarter_hour_checkpoint (checkpoint) VALUES ('09:30:00');
INSERT INTO quarter_hour_checkpoint (checkpoint) VALUES ('09:45:00');

INSERT INTO quarter_hour_checkpoint (checkpoint) VALUES ('10:00:00');
INSERT INTO quarter_hour_checkpoint (checkpoint) VALUES ('10:15:00');
INSERT INTO quarter_hour_checkpoint (checkpoint) VALUES ('10:30:00');
INSERT INTO quarter_hour_checkpoint (checkpoint) VALUES ('10:45:00');

INSERT INTO quarter_hour_checkpoint (checkpoint) VALUES ('11:00:00');
INSERT INTO quarter_hour_checkpoint (checkpoint) VALUES ('11:15:00');
INSERT INTO quarter_hour_checkpoint (checkpoint) VALUES ('11:30:00');
INSERT INTO quarter_hour_checkpoint (checkpoint) VALUES ('11:45:00');

INSERT INTO quarter_hour_checkpoint (checkpoint) VALUES ('12:00:00');
INSERT INTO quarter_hour_checkpoint (checkpoint) VALUES ('12:15:00');
INSERT INTO quarter_hour_checkpoint (checkpoint) VALUES ('12:30:00');
INSERT INTO quarter_hour_checkpoint (checkpoint) VALUES ('12:45:00');

INSERT INTO quarter_hour_checkpoint (checkpoint) VALUES ('13:00:00');
INSERT INTO quarter_hour_checkpoint (checkpoint) VALUES ('13:15:00');
INSERT INTO quarter_hour_checkpoint (checkpoint) VALUES ('13:30:00');
INSERT INTO quarter_hour_checkpoint (checkpoint) VALUES ('13:45:00');

INSERT INTO quarter_hour_checkpoint (checkpoint) VALUES ('14:00:00');
INSERT INTO quarter_hour_checkpoint (checkpoint) VALUES ('14:15:00');
INSERT INTO quarter_hour_checkpoint (checkpoint) VALUES ('14:30:00');
INSERT INTO quarter_hour_checkpoint (checkpoint) VALUES ('14:45:00');

INSERT INTO quarter_hour_checkpoint (checkpoint) VALUES ('15:00:00');
INSERT INTO quarter_hour_checkpoint (checkpoint) VALUES ('15:15:00');
INSERT INTO quarter_hour_checkpoint (checkpoint) VALUES ('15:30:00');
INSERT INTO quarter_hour_checkpoint (checkpoint) VALUES ('15:45:00');

INSERT INTO quarter_hour_checkpoint (checkpoint) VALUES ('16:00:00');
INSERT INTO quarter_hour_checkpoint (checkpoint) VALUES ('16:15:00');
INSERT INTO quarter_hour_checkpoint (checkpoint) VALUES ('16:30:00');
INSERT INTO quarter_hour_checkpoint (checkpoint) VALUES ('16:45:00');

INSERT INTO quarter_hour_checkpoint (checkpoint) VALUES ('17:00:00');
INSERT INTO quarter_hour_checkpoint (checkpoint) VALUES ('17:15:00');
INSERT INTO quarter_hour_checkpoint (checkpoint) VALUES ('17:30:00');
INSERT INTO quarter_hour_checkpoint (checkpoint) VALUES ('17:45:00');

INSERT INTO quarter_hour_checkpoint (checkpoint) VALUES ('18:00:00');
INSERT INTO quarter_hour_checkpoint (checkpoint) VALUES ('18:15:00');
INSERT INTO quarter_hour_checkpoint (checkpoint) VALUES ('18:30:00');
INSERT INTO quarter_hour_checkpoint (checkpoint) VALUES ('18:45:00');

INSERT INTO quarter_hour_checkpoint (checkpoint) VALUES ('19:00:00');
INSERT INTO quarter_hour_checkpoint (checkpoint) VALUES ('19:15:00');
INSERT INTO quarter_hour_checkpoint (checkpoint) VALUES ('19:30:00');
INSERT INTO quarter_hour_checkpoint (checkpoint) VALUES ('19:45:00');

INSERT INTO quarter_hour_checkpoint (checkpoint) VALUES ('20:00:00');
INSERT INTO quarter_hour_checkpoint (checkpoint) VALUES ('20:15:00');
INSERT INTO quarter_hour_checkpoint (checkpoint) VALUES ('20:30:00');
INSERT INTO quarter_hour_checkpoint (checkpoint) VALUES ('20:45:00');

INSERT INTO quarter_hour_checkpoint (checkpoint) VALUES ('21:00:00');
INSERT INTO quarter_hour_checkpoint (checkpoint) VALUES ('21:15:00');
INSERT INTO quarter_hour_checkpoint (checkpoint) VALUES ('21:30:00');
INSERT INTO quarter_hour_checkpoint (checkpoint) VALUES ('21:45:00');

INSERT INTO quarter_hour_checkpoint (checkpoint) VALUES ('22:00:00');
INSERT INTO quarter_hour_checkpoint (checkpoint) VALUES ('22:15:00');
INSERT INTO quarter_hour_checkpoint (checkpoint) VALUES ('22:30:00');
INSERT INTO quarter_hour_checkpoint (checkpoint) VALUES ('22:45:00');

INSERT INTO quarter_hour_checkpoint (checkpoint) VALUES ('23:00:00');
INSERT INTO quarter_hour_checkpoint (checkpoint) VALUES ('23:15:00');
INSERT INTO quarter_hour_checkpoint (checkpoint) VALUES ('23:30:00');
INSERT INTO quarter_hour_checkpoint (checkpoint) VALUES ('23:45:00');

CREATE TABLE hour_checkpoint ( 
	id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	checkpoint TIME NOT NULL UNIQUE 
);

INSERT INTO hour_checkpoint (checkpoint) VALUES ('00:00:00');
INSERT INTO hour_checkpoint (checkpoint) VALUES ('01:00:00');
INSERT INTO hour_checkpoint (checkpoint) VALUES ('02:00:00');
INSERT INTO hour_checkpoint (checkpoint) VALUES ('03:00:00');
INSERT INTO hour_checkpoint (checkpoint) VALUES ('04:00:00');
INSERT INTO hour_checkpoint (checkpoint) VALUES ('05:00:00');
INSERT INTO hour_checkpoint (checkpoint) VALUES ('06:00:00');
INSERT INTO hour_checkpoint (checkpoint) VALUES ('07:00:00');
INSERT INTO hour_checkpoint (checkpoint) VALUES ('08:00:00');
INSERT INTO hour_checkpoint (checkpoint) VALUES ('09:00:00');
INSERT INTO hour_checkpoint (checkpoint) VALUES ('10:00:00');
INSERT INTO hour_checkpoint (checkpoint) VALUES ('11:00:00');
INSERT INTO hour_checkpoint (checkpoint) VALUES ('12:00:00');
INSERT INTO hour_checkpoint (checkpoint) VALUES ('13:00:00');
INSERT INTO hour_checkpoint (checkpoint) VALUES ('14:00:00');
INSERT INTO hour_checkpoint (checkpoint) VALUES ('15:00:00');
INSERT INTO hour_checkpoint (checkpoint) VALUES ('16:00:00');
INSERT INTO hour_checkpoint (checkpoint) VALUES ('17:00:00');
INSERT INTO hour_checkpoint (checkpoint) VALUES ('18:00:00');
INSERT INTO hour_checkpoint (checkpoint) VALUES ('19:00:00');
INSERT INTO hour_checkpoint (checkpoint) VALUES ('20:00:00');
INSERT INTO hour_checkpoint (checkpoint) VALUES ('21:00:00');
INSERT INTO hour_checkpoint (checkpoint) VALUES ('22:00:00');
INSERT INTO hour_checkpoint (checkpoint) VALUES ('23:00:00');

CREATE TABLE weekly_checkpoint ( 
	id BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	starting_date DATE NOT NULL UNIQUE,
	ending_date DATE NOT NULL UNIQUE
);

CREATE TABLE monthly_checkpoint (
	id BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	checkpoint_date DATE NOT NULL UNIQUE
);

CREATE TABLE user (
	id	BIGINT NOT NULL PRIMARY KEY,
	name VARCHAR(100) NOT NULL,
	device_fingerprint_enabled BOOLEAN NOT NULL,
	is_master BOOLEAN NOT NULL,
	is_monitoring_enabled BOOLEAN NOT NULL DEFAULT 1,
	is_exchange BOOLEAN NOT NULL DEFAULT 0,
	fillColor_RGBA	varchar(15) default '220,220,220,0.2',
	highlightFill_RGBA	varchar(15) default '220,220,220,1',
	highlightStroke_RGBA	varchar(15) default '220,220,220,1'
);

CREATE TABLE trans_quarterly_monitor (
	id BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	user_id BIGINT NOT NULL,
	calendar_id BIGINT NOT NULL,
	quarter_hour_checkpoint_id INT NOT NULL,
	quantity INT NOT NULL DEFAULT 0,
	amount NUMERIC(15, 2) NOT NULL DEFAULT 0
);

ALTER TABLE trans_quarterly_monitor ADD FOREIGN KEY (user_id) REFERENCES user(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE trans_quarterly_monitor ADD FOREIGN KEY (calendar_id) REFERENCES calendar(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE trans_quarterly_monitor ADD FOREIGN KEY (quarter_hour_checkpoint_id) REFERENCES quarter_hour_checkpoint(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE trans_quarterly_monitor ADD UNIQUE INDEX index001 (user_id, calendar_id, quarter_hour_checkpoint_id);

CREATE TABLE trans_hourly_monitor (
	id BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	user_id BIGINT NOT NULL,
	calendar_id BIGINT NOT NULL,
	hour_checkpoint_id INT NOT NULL,
	quantity INT NOT NULL DEFAULT 0,
	amount NUMERIC(15, 2) NOT NULL DEFAULT 0
);

ALTER TABLE trans_hourly_monitor ADD FOREIGN KEY (user_id) REFERENCES user(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE trans_hourly_monitor ADD FOREIGN KEY (calendar_id) REFERENCES calendar(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE trans_hourly_monitor ADD FOREIGN KEY (hour_checkpoint_id) REFERENCES hour_checkpoint(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE trans_hourly_monitor ADD UNIQUE INDEX index001 (user_id, calendar_id, hour_checkpoint_id);

CREATE TABLE trans_daily_monitor (
	id BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	user_id BIGINT NOT NULL,
	date_id BIGINT NOT NULL,
	quantity INT NOT NULL DEFAULT 0,
	amount NUMERIC(15, 2) NOT NULL DEFAULT 0
);

ALTER TABLE trans_daily_monitor ADD FOREIGN KEY (user_id) REFERENCES user(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE trans_daily_monitor ADD FOREIGN KEY (date_id) REFERENCES calendar(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE trans_daily_monitor ADD UNIQUE INDEX index001 (user_id, date_id);

CREATE TABLE trans_weekly_monitor (
	id BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	user_id BIGINT NOT NULL,
	weekly_checkpoint_id BIGINT NOT NULL,
	quantity INT NOT NULL DEFAULT 0,
	amount NUMERIC(15, 2) NOT NULL DEFAULT 0
);

ALTER TABLE trans_weekly_monitor ADD FOREIGN KEY (user_id) REFERENCES user(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE trans_weekly_monitor ADD FOREIGN KEY (weekly_checkpoint_id) REFERENCES weekly_checkpoint(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE trans_weekly_monitor ADD UNIQUE INDEX index001 (user_id, weekly_checkpoint_id);

CREATE TABLE trans_monthly_monitor (
	id BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	user_id BIGINT NOT NULL,
	monthly_checkpoint_id BIGINT NOT NULL,
	quantity INT NOT NULL DEFAULT 0,
	amount NUMERIC(15, 2) NOT NULL DEFAULT 0
);

ALTER TABLE trans_monthly_monitor ADD FOREIGN KEY (user_id) REFERENCES user(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE trans_monthly_monitor ADD FOREIGN KEY (monthly_checkpoint_id) REFERENCES monthly_checkpoint(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE trans_monthly_monitor ADD UNIQUE INDEX index001 (user_id, monthly_checkpoint_id);

CREATE TABLE analysis_quarterly_monitor (
	id BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	user_id BIGINT NOT NULL,
	calendar_id BIGINT NOT NULL,
	quarter_hour_checkpoint_id INT NOT NULL,
	automatic_analysis BIGINT NOT NULL DEFAULT 0,
	manual_analysys BIGINT NOT NULL DEFAULT 0
);
ALTER TABLE analysis_quarterly_monitor ADD FOREIGN KEY (user_id) REFERENCES user(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE analysis_quarterly_monitor ADD FOREIGN KEY (calendar_id) REFERENCES calendar(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE analysis_quarterly_monitor ADD FOREIGN KEY (quarter_hour_checkpoint_id) REFERENCES quarter_hour_checkpoint(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE analysis_quarterly_monitor ADD UNIQUE INDEX index001 (user_id, calendar_id, quarter_hour_checkpoint_id);


CREATE TABLE fingerprint_quarterly_monitor (
	id BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	user_id BIGINT NOT NULL,
	calendar_id BIGINT NOT NULL,
	quarter_hour_checkpoint_id INT NOT NULL,
	quantity BIGINT NOT NULL DEFAULT 0
);
ALTER TABLE fingerprint_quarterly_monitor ADD FOREIGN KEY (user_id) REFERENCES user(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE fingerprint_quarterly_monitor ADD FOREIGN KEY (calendar_id) REFERENCES calendar(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE fingerprint_quarterly_monitor ADD FOREIGN KEY (quarter_hour_checkpoint_id) REFERENCES quarter_hour_checkpoint(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE fingerprint_quarterly_monitor ADD UNIQUE INDEX index001 (user_id, calendar_id, quarter_hour_checkpoint_id);
