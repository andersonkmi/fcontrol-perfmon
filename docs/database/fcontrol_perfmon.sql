-- MySQL dump 10.14  Distrib 5.5.41-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: fcontrol_perfmon
-- ------------------------------------------------------
-- Server version	5.5.41-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `calendar`
--

DROP TABLE IF EXISTS `calendar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `calendar` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `calendar_date` date NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `calendar_date` (`calendar_date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `calendar`
--

LOCK TABLES `calendar` WRITE;
/*!40000 ALTER TABLE `calendar` DISABLE KEYS */;
/*!40000 ALTER TABLE `calendar` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `daily_checkpoint`
--

DROP TABLE IF EXISTS `daily_checkpoint`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `daily_checkpoint` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `daily_checkpoint` date NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `daily_checkpoint` (`daily_checkpoint`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `daily_checkpoint`
--

LOCK TABLES `daily_checkpoint` WRITE;
/*!40000 ALTER TABLE `daily_checkpoint` DISABLE KEYS */;
/*!40000 ALTER TABLE `daily_checkpoint` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hour_checkpoint`
--

DROP TABLE IF EXISTS `hour_checkpoint`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hour_checkpoint` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `checkpoint` time NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `checkpoint` (`checkpoint`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hour_checkpoint`
--

LOCK TABLES `hour_checkpoint` WRITE;
/*!40000 ALTER TABLE `hour_checkpoint` DISABLE KEYS */;
INSERT INTO `hour_checkpoint` VALUES (1,'00:00:00'),(2,'01:00:00'),(3,'02:00:00'),(4,'03:00:00'),(5,'04:00:00'),(6,'05:00:00'),(7,'06:00:00'),(8,'07:00:00'),(9,'08:00:00'),(10,'09:00:00'),(11,'10:00:00'),(12,'11:00:00'),(13,'12:00:00'),(14,'13:00:00'),(15,'14:00:00'),(16,'15:00:00'),(17,'16:00:00'),(18,'17:00:00'),(19,'18:00:00'),(20,'19:00:00'),(21,'20:00:00'),(22,'21:00:00'),(23,'22:00:00'),(24,'23:00:00');
/*!40000 ALTER TABLE `hour_checkpoint` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `monthly_checkpoint`
--

DROP TABLE IF EXISTS `monthly_checkpoint`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `monthly_checkpoint` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `checkpoint_date` date NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `checkpoint_date` (`checkpoint_date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `monthly_checkpoint`
--

LOCK TABLES `monthly_checkpoint` WRITE;
/*!40000 ALTER TABLE `monthly_checkpoint` DISABLE KEYS */;
/*!40000 ALTER TABLE `monthly_checkpoint` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `quarter_hour_checkpoint`
--

DROP TABLE IF EXISTS `quarter_hour_checkpoint`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quarter_hour_checkpoint` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `checkpoint` time NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `checkpoint` (`checkpoint`)
) ENGINE=InnoDB AUTO_INCREMENT=97 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `quarter_hour_checkpoint`
--

LOCK TABLES `quarter_hour_checkpoint` WRITE;
/*!40000 ALTER TABLE `quarter_hour_checkpoint` DISABLE KEYS */;
INSERT INTO `quarter_hour_checkpoint` VALUES (1,'00:00:00'),(2,'00:15:00'),(3,'00:30:00'),(4,'00:45:00'),(5,'01:00:00'),(6,'01:15:00'),(7,'01:30:00'),(8,'01:45:00'),(9,'02:00:00'),(10,'02:15:00'),(11,'02:30:00'),(12,'02:45:00'),(13,'03:00:00'),(14,'03:15:00'),(15,'03:30:00'),(16,'03:45:00'),(17,'04:00:00'),(18,'04:15:00'),(19,'04:30:00'),(20,'04:45:00'),(21,'05:00:00'),(22,'05:15:00'),(23,'05:30:00'),(24,'05:45:00'),(25,'06:00:00'),(26,'06:15:00'),(27,'06:30:00'),(28,'06:45:00'),(29,'07:00:00'),(30,'07:15:00'),(31,'07:30:00'),(32,'07:45:00'),(33,'08:00:00'),(34,'08:15:00'),(35,'08:30:00'),(36,'08:45:00'),(37,'09:00:00'),(38,'09:15:00'),(39,'09:30:00'),(40,'09:45:00'),(41,'10:00:00'),(42,'10:15:00'),(43,'10:30:00'),(44,'10:45:00'),(45,'11:00:00'),(46,'11:15:00'),(47,'11:30:00'),(48,'11:45:00'),(49,'12:00:00'),(50,'12:15:00'),(51,'12:30:00'),(52,'12:45:00'),(53,'13:00:00'),(54,'13:15:00'),(55,'13:30:00'),(56,'13:45:00'),(57,'14:00:00'),(58,'14:15:00'),(59,'14:30:00'),(60,'14:45:00'),(61,'15:00:00'),(62,'15:15:00'),(63,'15:30:00'),(64,'15:45:00'),(65,'16:00:00'),(66,'16:15:00'),(67,'16:30:00'),(68,'16:45:00'),(69,'17:00:00'),(70,'17:15:00'),(71,'17:30:00'),(72,'17:45:00'),(73,'18:00:00'),(74,'18:15:00'),(75,'18:30:00'),(76,'18:45:00'),(77,'19:00:00'),(78,'19:15:00'),(79,'19:30:00'),(80,'19:45:00'),(81,'20:00:00'),(82,'20:15:00'),(83,'20:30:00'),(84,'20:45:00'),(85,'21:00:00'),(86,'21:15:00'),(87,'21:30:00'),(88,'21:45:00'),(89,'22:00:00'),(90,'22:15:00'),(91,'22:30:00'),(92,'22:45:00'),(93,'23:00:00'),(94,'23:15:00'),(95,'23:30:00'),(96,'23:45:00');
/*!40000 ALTER TABLE `quarter_hour_checkpoint` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trans_daily_monitor`
--

DROP TABLE IF EXISTS `trans_daily_monitor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trans_daily_monitor` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `daily_checkpoint_id` bigint(20) NOT NULL,
  `quantity` int(11) NOT NULL DEFAULT '0',
  `amount` decimal(15,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index001` (`user_id`,`daily_checkpoint_id`),
  KEY `daily_checkpoint_id` (`daily_checkpoint_id`),
  CONSTRAINT `trans_daily_monitor_ibfk_2` FOREIGN KEY (`daily_checkpoint_id`) REFERENCES `daily_checkpoint` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `trans_daily_monitor_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trans_daily_monitor`
--

LOCK TABLES `trans_daily_monitor` WRITE;
/*!40000 ALTER TABLE `trans_daily_monitor` DISABLE KEYS */;
/*!40000 ALTER TABLE `trans_daily_monitor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trans_hourly_monitor`
--

DROP TABLE IF EXISTS `trans_hourly_monitor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trans_hourly_monitor` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `calendar_id` bigint(20) NOT NULL,
  `hour_checkpoint_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL DEFAULT '0',
  `amount` decimal(15,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index001` (`user_id`,`calendar_id`,`hour_checkpoint_id`),
  KEY `calendar_id` (`calendar_id`),
  KEY `hour_checkpoint_id` (`hour_checkpoint_id`),
  CONSTRAINT `trans_hourly_monitor_ibfk_3` FOREIGN KEY (`hour_checkpoint_id`) REFERENCES `hour_checkpoint` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `trans_hourly_monitor_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `trans_hourly_monitor_ibfk_2` FOREIGN KEY (`calendar_id`) REFERENCES `calendar` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trans_hourly_monitor`
--

LOCK TABLES `trans_hourly_monitor` WRITE;
/*!40000 ALTER TABLE `trans_hourly_monitor` DISABLE KEYS */;
/*!40000 ALTER TABLE `trans_hourly_monitor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trans_monthly_monitor`
--

DROP TABLE IF EXISTS `trans_monthly_monitor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trans_monthly_monitor` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `monthly_checkpoint_id` bigint(20) NOT NULL,
  `quantity` int(11) NOT NULL DEFAULT '0',
  `amount` decimal(15,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index001` (`user_id`,`monthly_checkpoint_id`),
  KEY `monthly_checkpoint_id` (`monthly_checkpoint_id`),
  CONSTRAINT `trans_monthly_monitor_ibfk_2` FOREIGN KEY (`monthly_checkpoint_id`) REFERENCES `monthly_checkpoint` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `trans_monthly_monitor_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trans_monthly_monitor`
--

LOCK TABLES `trans_monthly_monitor` WRITE;
/*!40000 ALTER TABLE `trans_monthly_monitor` DISABLE KEYS */;
/*!40000 ALTER TABLE `trans_monthly_monitor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trans_quarterly_monitor`
--

DROP TABLE IF EXISTS `trans_quarterly_monitor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trans_quarterly_monitor` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `calendar_id` bigint(20) NOT NULL,
  `quarter_hour_checkpoint_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL DEFAULT '0',
  `amount` decimal(15,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index001` (`user_id`,`calendar_id`,`quarter_hour_checkpoint_id`),
  KEY `calendar_id` (`calendar_id`),
  KEY `quarter_hour_checkpoint_id` (`quarter_hour_checkpoint_id`),
  CONSTRAINT `trans_quarterly_monitor_ibfk_3` FOREIGN KEY (`quarter_hour_checkpoint_id`) REFERENCES `quarter_hour_checkpoint` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `trans_quarterly_monitor_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `trans_quarterly_monitor_ibfk_2` FOREIGN KEY (`calendar_id`) REFERENCES `calendar` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trans_quarterly_monitor`
--

LOCK TABLES `trans_quarterly_monitor` WRITE;
/*!40000 ALTER TABLE `trans_quarterly_monitor` DISABLE KEYS */;
/*!40000 ALTER TABLE `trans_quarterly_monitor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trans_weekly_monitor`
--

DROP TABLE IF EXISTS `trans_weekly_monitor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trans_weekly_monitor` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `weekly_checkpoint_id` bigint(20) NOT NULL,
  `quantity` int(11) NOT NULL DEFAULT '0',
  `amount` decimal(15,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index001` (`user_id`,`weekly_checkpoint_id`),
  KEY `weekly_checkpoint_id` (`weekly_checkpoint_id`),
  CONSTRAINT `trans_weekly_monitor_ibfk_2` FOREIGN KEY (`weekly_checkpoint_id`) REFERENCES `weekly_checkpoint` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `trans_weekly_monitor_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trans_weekly_monitor`
--

LOCK TABLES `trans_weekly_monitor` WRITE;
/*!40000 ALTER TABLE `trans_weekly_monitor` DISABLE KEYS */;
/*!40000 ALTER TABLE `trans_weekly_monitor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` bigint(20) NOT NULL,
  `name` varchar(100) NOT NULL,
  `device_fingerprint_enabled` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `weekly_checkpoint`
--

DROP TABLE IF EXISTS `weekly_checkpoint`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `weekly_checkpoint` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `starting_date` date NOT NULL,
  `ending_date` date NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `starting_date` (`starting_date`),
  UNIQUE KEY `ending_date` (`ending_date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `weekly_checkpoint`
--

LOCK TABLES `weekly_checkpoint` WRITE;
/*!40000 ALTER TABLE `weekly_checkpoint` DISABLE KEYS */;
/*!40000 ALTER TABLE `weekly_checkpoint` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-03-07 16:14:09
